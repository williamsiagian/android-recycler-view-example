package com.example.recyclerview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.recyclerview.adapters.DataModel
import com.example.recyclerview.adapters.ItemAdapter
import kotlinx.android.synthetic.main.activity_main.*


// RecyclerView with manual data
// https://www.youtube.com/watch?v=oDfl-xLXiac&list=PLwhVruPHD9rz2MwQuYSenQ0WK1IGP5rYM&index=6

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recycler_view_items.layoutManager = LinearLayoutManager(this)

        //Horizontal scroll
//        recycler_view_items.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)

        val itemAdapter = ItemAdapter(this, getItemList())

        recycler_view_items.adapter = itemAdapter


    }

    private fun getItemList() : ArrayList<DataModel>{

        val list = ArrayList<DataModel>()

        list.add(DataModel("Item 1 ViewType 1", ItemAdapter.VIEW_TYPE_ONE))
        list.add(DataModel("Item 2 ViewType 2", ItemAdapter.VIEW_TYPE_TWO))
        list.add(DataModel("Item 3 ViewType 1", ItemAdapter.VIEW_TYPE_ONE))
        list.add(DataModel("Item 4 ViewType 2", ItemAdapter.VIEW_TYPE_TWO))
        list.add(DataModel("Item 5 ViewType 1", ItemAdapter.VIEW_TYPE_ONE))
        list.add(DataModel("Item 6 ViewType 1", ItemAdapter.VIEW_TYPE_ONE))
        list.add(DataModel("Item 7 ViewType 2", ItemAdapter.VIEW_TYPE_TWO))
        list.add(DataModel("Item 8 ViewType 1", ItemAdapter.VIEW_TYPE_ONE))
        list.add(DataModel("Item 9 ViewType 2", ItemAdapter.VIEW_TYPE_TWO))
        list.add(DataModel("Item 10 ViewType 2", ItemAdapter.VIEW_TYPE_TWO))
        list.add(DataModel("Item 11 ViewType 1", ItemAdapter.VIEW_TYPE_ONE))
        list.add(DataModel("Item 12 ViewType 2", ItemAdapter.VIEW_TYPE_TWO))

        return list

    }


//    private fun getItemList() : ArrayList<String> {
//        val list = ArrayList<String>()
//
//        for(i in 1..15){
//            list.add("Item $i")
//        }
//
//        return list
//
//    }




}